package slotmachine.ui.view;

public interface IReelAdapter {
    int getCount();

    IView getView(int position);
}
