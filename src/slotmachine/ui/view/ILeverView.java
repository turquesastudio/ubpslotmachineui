package slotmachine.ui.view;

import slotmachine.ui.handler.IPlayHandler;

public interface ILeverView extends IView {
    void setPlayHandler(IPlayHandler handler);
}
