package slotmachine.ui.view;

import java.awt.*;

public interface IView {
    Component getComponent();
}
