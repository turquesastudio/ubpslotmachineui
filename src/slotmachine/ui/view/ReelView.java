package slotmachine.ui.view;

import slotmachine.ui.handler.IReelHandler;

import javax.swing.*;
import java.awt.*;

class ReelView implements IReelView, IReelHandler {
    private IReelAdapter reelAdapter;
    private JPanel panel;

    public ReelView() {
        panel = new JPanel();
    }

    @Override
    public Component getComponent() {
        return panel;
    }

    @Override
    public void setAdapter(IReelAdapter reelAdapter) {
        this.reelAdapter = reelAdapter;

        panel.setLayout(new GridLayout(1, reelAdapter.getCount()));

        update();
    }

    @Override
    public void update() {
        panel.removeAll();

        for (int i = 0; i < reelAdapter.getCount(); i++) {
            panel.add(reelAdapter.getView(i).getComponent());
        }

        panel.revalidate();
    }
}
