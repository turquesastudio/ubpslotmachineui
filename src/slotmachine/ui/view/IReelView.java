package slotmachine.ui.view;

public interface IReelView extends IView {
    void setAdapter(IReelAdapter reelAdapter);
}
