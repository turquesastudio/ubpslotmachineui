package slotmachine.ui.handler;

import slotmachine.ui.data.ICredit;

public interface ICreditHandler {
    void addCredit(ICredit credit);
}
