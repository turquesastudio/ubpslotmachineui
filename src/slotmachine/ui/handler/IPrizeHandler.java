package slotmachine.ui.handler;

public interface IPrizeHandler {
    void retrieve(int prize);
}
